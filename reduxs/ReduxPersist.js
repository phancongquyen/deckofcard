import storage from 'redux-persist/lib/storage'; // default: localStorage if web, AsyncStorage if react-native

const REDUX_PERSIST = {
	active: true,
	reducerVersion: '1.0.1',
	storeConfig: {
		key: 'primary',
		blacklist: [],
		storage,
	}
}

export default REDUX_PERSIST