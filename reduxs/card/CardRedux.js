import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';


const { Types, Creators } = createActions({

    getDeckOfCardRequest: ['classify', 'params'],
    getDeckOfCardSuccess: ['classify', 'payload'],

    chooseCardRequest: ['classify', 'params'],
    chooseCardSuccess: ['classify', 'payload'],

    commonActionFailure: ['classify', 'error'],

});

export const CardTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    error: {},
    fetching: {},
    contents: {},
});

// START:  DeckOfCard action
export const getDeckOfCardRequest = (state, { classify }) => {
    return {
        ...state,
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
        contents: {},
    }
};

export const getDeckOfCardSuccess = (state, { classify, payload }) => {
    return {
        ...state,
        fetching: { ...state.fetching, [classify]: false },
        contents: { ...state.contents, [classify]: payload.deck_id },
    }
}
// END: DeckOfCard action

// START:  chooseCard
export const chooseCardRequest = (state, { classify }) => {
    return {
        ...state,
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
    }
};

export const chooseCardSuccess = (state, { classify, payload }) => {
    let contents = Immutable.asMutable(state.contents, { deep: true });
    contents[classify] = payload.cards[0];
    if (!contents['lstCard']) {
        contents['lstCard'] = [];
    }
    contents['lstCard'].unshift({
        ...payload.cards[0],
        timePush: new Date().getTime()
    })
    if (contents['lstCard'].length > 3) {
        contents['lstCard'].pop();
    }
    return {
        ...state,
        fetching: { ...state.fetching, [classify]: false },
        contents: contents,
    }
}
// END: chooseCard

export const commonActionFailure = (state, { classify, error }) => {
    return {
        ...state,
        fetching: { ...state.fetching, [classify]: false },
        error: { ...state.error, [classify]: error },
    }
}



export const reducer = createReducer(INITIAL_STATE, {
    [Types.COMMON_ACTION_FAILURE]: commonActionFailure,

    [Types.GET_DECK_OF_CARD_REQUEST]: getDeckOfCardRequest,
    [Types.GET_DECK_OF_CARD_SUCCESS]: getDeckOfCardSuccess,

    [Types.CHOOSE_CARD_REQUEST]: chooseCardRequest,
    [Types.CHOOSE_CARD_SUCCESS]: chooseCardSuccess,

});