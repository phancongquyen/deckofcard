import React from 'react';
import App from 'next/app';
import { PersistGate } from 'redux-persist/integration/react';
import { ReactReduxContext } from 'react-redux';

import reduxWrapper from '../reduxs/index';

import RootContainer from '../container/root';

import '../styles/globals.css'
class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <ReactReduxContext.Consumer>
        {({ store }) => (
          <PersistGate persistor={store.__persistor} loading={null}>
            <RootContainer>
              <Component {...pageProps} />
            </RootContainer>
          </PersistGate>
        )}
      </ReactReduxContext.Consumer>
    );
  }
}

export default reduxWrapper.withRedux(MyApp);