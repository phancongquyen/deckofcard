import React, { Component } from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

import Button from '@material-ui/core/Button';

class MiddleComponent extends Component {
    render() {
        const { classes, name, card } = this.props;
        return (
            <div className={classes.container}>
                <div className={classes.header}>
                    <span>{`Hi ${name || ''},`}</span>
                    <span>Let’s Play! 👋</span>
                </div>
                <div className={classes.contentCenter}>
                    <div className={classes.deckOfCard}>
                        {card && <img src={card.image} className={classes.imgCard}/>}
                    </div>
                </div>
                <div className={classes.contentCenter}>
                    <Button className={classes.customBtn} onClick={() => this.props.chooseCard()}>
                        <span>Choose a card!</span>
                    </Button>
                </div>
            </div>
        )
    }
}

MiddleComponent.defaultProps = {
    chooseCard: () => null,
    name: 'fname'
};

export default withStyles(styles)(MiddleComponent);