export const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        borderRight: '1px solid #E4E4E4',
    },
    header: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 48,
        marginBottom: 52,
        marginLeft: 65,
        '& span': {
            color: '#11142D'
        },
        '& span:first-child': {
            fontSize: 24,
            fontWeight: 500,
            lineHeight: '32px',
            textTransform: 'capitalize',
        },
        '& span:last-of-type': {
            fontSize: 48,
            fontWeight: 600,
            lineHeight: '72px',
        },
    },
    contentCenter: {
        display: 'flex',
        justifyContent: 'center',
    },
    deckOfCard: {
        width: 350,
        minHeight: 480,
        height: 'fit-content',
        marginBottom: 57,
        background: '#6C5DD3',
        borderRadius: 24,
        boxShadow: '0px 16px 0px 0px #CFC8FF',
    },
    imgCard: {
        width: '-webkit-fill-available',
        height: 'fit-content'
    },
    customBtn: {
        display: 'flex',
        width: 306,
        height: 56,
        margin: '0 20px',
        background: '#1B1D21',
        borderRadius: 12,
        justifyContent: 'center',
        textTransform: 'none',
        '&:hover': {
            background: '#1b1d21cc',
        },
        '& span': {
            fontSize: 14,
            fontWeight: 600,
            color: '#fff',
        }
    }
});