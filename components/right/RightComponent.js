import React, { Component, Fragment } from 'react';
import moment from 'moment';

import ArrowForward from '@material-ui/icons/ArrowForwardIos';

import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';


const isArray = (value, minLength) => Array.isArray(value) && (minLength ? (value.length >= (typeof minLength === 'number' ? minLength : 1)) : true);

class RightComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    _renderCards = () => {
        const { classes, lstCard } = this.props;
        return (
            <Fragment>
                {isArray(lstCard, true) && lstCard.map((card, idx) => {
                    return (
                        <div key={idx} className={classes.rootCard} onClick={()=>{this.props.setCurrCard(card)}}>
                            <div>
                                <img className={classes.imgCard} src={card.image} />
                            </div>
                            <div className={classes.orderTxt}>
                                <span>{card.code}</span>
                                <span>{moment(card.timePush).format('MM/DD/YYYY HH:MM')}</span>
                            </div>
                            <div>
                                <ArrowForward className={classes.btnIcon}/>
                            </div>
                        </div>
                    );
                })}
            </Fragment>
        );
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.container}>
                <div className={classes.boxContent}>
                    <span className={classes.txtTitle}>Your last cards</span>
                    {this._renderCards()}
                </div>
            </div>
        )
    }
}

RightComponent.defaultProps = {
    lstCard: []
};

export default withStyles(styles)(RightComponent);