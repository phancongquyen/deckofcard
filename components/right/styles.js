export const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column'
    },
    boxContent: {
        width: 285,
        minHeight: 420,
        margin: '227px 0 57px 66px',
        padding: '32px 0',
        background: '#fff',
        borderRadius: 24,
        WebkitBoxShadow: '1px 1px 20px 0px rgb(73 73 73 / 16%)',
    },
    txtTitle: {
        marginLeft: 32,
        marginBottom: 8,
        fontSize: 18,
        fontWeight: 500,
        lineHeight: '24px',
        color: '#11142D'
    },
    rootCard: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 32,
        '&:hover': {
            background: '#e8e9eccc',
            cursor: 'pointer'
        },
    },
    imgCard: {
        width: 56,
        height: 'fit-content',
        display: 'flex',
        alignItems: 'center',
        marginRight: 16,
        justifyContent: 'center',

    },
    orderTxt: {
        display: 'flex',
        flexDirection: 'column',
        '& span': {
            fontWeight: 600,
        },
        '& span:first-child': {
            fontSize: 14,
            color: '#11142D',
            textTransform: 'capitalize',
        },
        '& span:last-of-type': {
            fontSize: 13,
            color: '#808191'
        },
    },
    btnIcon: {
        fontSize: 16,
        marginLeft: 16,
        
    },
});