export const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: '100vh',
        borderRight: '1px solid #E4E4E4',
    },
    header: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 64,
        marginBottom: 32,
        '& img': {
            with: 32,
            height: 'fit-content'
        },
        '& span': {
            color: '#1B1464',
            fontSize: 36,
            fontWeight: 700,
        }
    },
    txtAdmin: {
        fontSize: 12,
        fontWeight: 500,
        color: '#808191',
        margin: '0 0 16px 40px'
    },
    restartBtn: {
        display: 'flex',
        width: '-webkit-fill-available',
        height: 56,
        margin: '0 20px',
        background: '#6C5DD3',
        borderRadius: 12,
        textTransform: 'capitalize',
        justifyContent: 'flex-start',
        '&:hover': {
            background: '#6c5dd3cc',
        },
        '& img': {
            width: 24,
            height: 24,
            margin: '0 16px'
        },
        '& span': {
            fontSize: 14,
            fontWeight: 600,
            color: '#fff',
        }
    },
    footer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: '0 0 30px 40px'
    },
    avatar: {
        width: 40,
        height: 40,
        display: 'flex',
        alignItems: 'center',
        marginRight: 16,
        borderRadius: '50%',
        justifyContent: 'center',
        backgroundColor: 'rgba(73,73,73,0.1)'
    },
    txtUser: {
        display: 'flex',
        flexDirection: 'column',
        '& span': {
            fontWeight: 600,
        },
        '& span:first-child': {
            fontSize: 14,
            color: '#11142D',
            textTransform: 'capitalize',
        },
        '& span:last-of-type': {
            fontSize: 13,
            color: '#808191'
        },

    }
});