import React, { Component } from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

import Button from '@material-ui/core/Button';

class LeftComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const { classes, name } = this.props;
        return (
            <div className={classes.container}>
                <div>
                    <div className={classes.header}>
                        <img src={'/icons/ic_open_door.png'} />
                        <span>simply.</span>
                    </div>
                    <p className={classes.txtAdmin}>Admin tools</p>
                    <Button className={classes.restartBtn} onClick={() => this.props.restartGame()}>
                        <img src={'/icons/ic_restart.png'} className={classes.icBtn} />
                        <span className={classes.txtBtn}>restart game</span>
                    </Button>
                </div>
                <div className={classes.footer}>
                    <div className={classes.avatar}></div>
                    <div className={classes.txtUser}>
                        <span>{name || ''}</span>
                        <span>Free account</span>
                    </div>
                </div>
            </div>
        )
    }
}

LeftComponent.defaultProps = {
    restartGame: () => null,
    name: '{fname}'
};

export default withStyles(styles)(LeftComponent);