import { doRequest } from "../../utils/CoreUtils";

export default {
    getDeckOfCard: () => {
        let url = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1';
        return doRequest('get', url);
    },

    chooseCard: (params) => {
        let url = `https://deckofcardsapi.com/api/deck/${params.deck_id}/draw/?count=1`;
        return doRequest('get', url);
    }
}