## Folder Structure

After creating an app, it should look something like:

```
.
├── README.md
├── components
│   ├── left
│   └── middle
│   └── right
├── container
│   ├── main
│   └── root
├── next.config.js
├── node_modules
│   ├── [...]
├── package.json
├── pages
│   └── _app.js     // page custom 
│   └── index.js
│   └── main.js
├── public
│   └── icon       // folder containing local icon 
│   └── favicon.ico
├── reduxs
│   ├── [...]
├── sagas
│   ├── [...]
├── services
│   ├── card        // API folder
├── utils           // Directory of reusable functions
│   ├── CoreUtils.js    
└── yarn.lock

```
Routing in Next.js is based on the file system, so `./pages/index.js` maps to the `/` route and
`./pages/about.js` would map to `/about`.

The `./public` directory maps to `/static` in the `next` server, so you can put all your
other static resources like images or compiled CSS in there.


Read more about [Next's Routing](https://github.com/zeit/next.js#routing)

## Available Scripts

In the project directory, you can run:

### `npm run dev` or `yarn dev`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any errors in the console.

### `npm run build` or `yarn build`

Builds the app for production to the `.next` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm run start` or `yarn start`

Starts the application in production mode.
The application should be compiled with \`next build\` first.

See the section in Next docs about [deployment](https://github.com/zeit/next.js/wiki/Deployment) for more information.

