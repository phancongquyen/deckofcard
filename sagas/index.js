import { all, takeLatest } from 'redux-saga/effects';

/* ------------- Types ------------- */
import { CardTypes } from '../reduxs/card/CardRedux';

/* ------------- Sagas ------------- */
import {
  getDeckOfCard, chooseCard
} from './card/CardSagas';

/* ------------- Connect all Sagas to root ------------- */

export default function* rootSaga() {
  yield all([
    takeLatest(CardTypes.GET_DECK_OF_CARD_REQUEST, getDeckOfCard),
    takeLatest(CardTypes.CHOOSE_CARD_REQUEST, chooseCard),
  ]);
}