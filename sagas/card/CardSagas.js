import { call, put, delay } from 'redux-saga/effects';

//action
import CardActions from '../../reduxs/card/CardRedux';

// services
import CardService from '../../services/card/DeckOfCardsAPI';


export function* getDeckOfCard(action) {
    const { classify } = action;
    try {
        let result = yield call(CardService.getDeckOfCard);
        if (result?.data) {
            yield put(CardActions.getDeckOfCardSuccess(classify, result.data));
        } else throw result;
    } catch (error) {
        yield put(CardActions.commonActionFailure(classify, error));
    }
}

export function* chooseCard(action) {
    const { classify, params } = action;
    try {
        let result = yield call(CardService.chooseCard, params);
        if (result?.data) {
            yield put(CardActions.chooseCardSuccess(classify, result.data));
        } else throw result;
    } catch (error) {
        yield put(CardActions.commonActionFailure(classify, error));
    }
}