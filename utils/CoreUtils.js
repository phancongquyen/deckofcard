
import axios from 'axios';

export function getRouterQuery() {
    return {
        ...Object.fromEntries(window.location.search.substring(1).split('&').map(param => {
            let parts = param.split('=');
            parts[0] = decodeURIComponent(parts[0]).trim();
            parts[1] = decodeURIComponent(parts[1]).trim();
            return parts;
        }).filter(i => i[0])),
    };
}

export const isJson = data => {
    try {
        if (!data) return false;
        if (typeof data === 'string') {
            JSON.parse(data);
        } else {
            JSON.parse(JSON.stringify(data));
        }
    } catch (e) { return false; }
    return true;
}

export const doRequest = (method, url, body) => {
    const reqConfig = {
        method,
        url: String(url),
        data: isJson(body) ? JSON.stringify(body) : (body || '{}'),
    }
    return axios.request(reqConfig);
};