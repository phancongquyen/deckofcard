import React, { Component } from 'react';
import MainContainer from '../main';

class RootContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
    }
    render() {
        return (
            <div>
                <MainContainer/>
            </div>
        )
    }
}
export default RootContainer;