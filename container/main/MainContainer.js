import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';


//actions - redux
import CardActions from '../../reduxs/card/CardRedux';

import Grid from '@material-ui/core/Grid';

import LeftComponent from '../../components/left'
import MidComponent from '../../components/middle';
import RightComponent from '../../components/right';

import { getRouterQuery } from '../../utils/CoreUtils'

class MainContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currCard: null
        };
    }

    componentDidMount() {
        const { name } = getRouterQuery();
        const { contents } = this.props;
        this.setState({ currCard: contents?.['card'] || null, name: name || null });
    }
    componentDidUpdate = (prevProps) => {
        const { fetching, contents } = this.props;
        if (prevProps.fetching['deckOfCard'] !== fetching['deckOfCard'] && !fetching['deckOfCard']) {
            this.setState({ currCard: null });
        }
        if (prevProps.fetching['card'] !== fetching['card'] && !fetching['card']) {
            this.setState({ currCard: contents['card'] });
        }
    }
    handelRestartGame = () => {
        this.props.restartGame('deckOfCard');
    }
    handelChooseCard = () => {
        const { contents } = this.props;
        if (contents['deckOfCard']) {
            this.props.chooseCard('card', { deck_id: contents['deckOfCard'] });
        } else {
            console.log("deckOfCard is empty, pls restart game!")
        }
    }

    handelSetCurrCard = (card) => {
        if (card) {
            delete card.timePush;
            this.setState({ currCard: card });
        }
    }
    render() {
        const { currCard, name } = this.state;
        const { contents } = this.props;
        return (
            <Grid container>
                <Grid item md={2} sm={12}>
                    <LeftComponent
                        name={name}
                        restartGame={() => this.handelRestartGame()}
                    />
                </Grid>
                <Grid item md={7} sm={12}>
                    <MidComponent
                        name={name}
                        chooseCard={() => this.handelChooseCard()}
                        card={currCard}
                    />
                </Grid>
                <Grid item md={3} sm={12}>
                    <RightComponent
                        lstCard={contents['lstCard']}
                        setCurrCard={(card) => this.handelSetCurrCard(card)}
                    />
                </Grid>
            </Grid>
        )
    }
}
const mapStateToProps = (state) => {
    const { contents, fetching } = state.cards;
    return { contents, fetching }
};

const mapDispatchToProps = dispatch => ({
    restartGame: (classify) => dispatch(CardActions.getDeckOfCardRequest(classify)),
    chooseCard: (classify, params) => dispatch(CardActions.chooseCardRequest(classify, params)),

});

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(MainContainer);
